<?php

/**
 * @file
 * Functions for database interactions.
 */

/**
 * Retrieve info about processed nodes.
 */
function activetl_db_processed_nodes_info_select() {
  $query = db_select('activetl_nodes', 'atl')
    ->fields('atl')
    ->fields('n', array('type'));
  $query->join('node', 'n', 'n.nid = atl.nid');
  $query = $query->execute();
  $result = $query->fetchAllAssoc('id');
  return $result;
}

/**
 * Retrieve info about one node.
 *
 * @param object $node
 *   Node to retrieve.
 * @param string $field
 *   Name of field to retrieve.
 */
function activetl_db_node_info_select($node, $field) {
  $query = db_select('activetl_nodes', 'nodes')
    ->fields('nodes')
    ->condition('nid', $node->nid)
    ->condition('field', $field)
    ->execute();
  $result = $query->fetchAssoc();
  return $result;
}

/**
 * Update active taxonomy links.
 *
 * @param array $values
 *   Node to update.
 * @param array $keys
 *   Name of field to update.
 */
function activetl_db_node_info_update(array $values, array $keys = array()) {
  $result = drupal_write_record('activetl_nodes', $values, $keys);
  return $result;
}

/**
 * Delete active taxonomy links.
 *
 * @param object $node
 *   Node to clean up form taxonomy links.
 * @param string $field
 *   Name of field to clean up form taxonomy links.
 */
function activetl_db_node_info_delete($node, $field) {
  $query = db_delete('activetl_nodes')
    ->condition('nid', $node->nid)
    ->condition('field', $field);
  $result = $query->execute();
  return $result;
}

/**
 * Retrieve info about unprocessed nodes.
 *
 * @param array $content_types
 *   Content types that need to check.
 */
function activetl_db_unprocessed_nodes_select(array $content_types) {
  $result = array();
  if (!empty($content_types)) {
    $query = db_select('node', 'n')
      ->fields('n', array('nid', 'type'))
      ->condition('n.type', $content_types, 'IN');
    $query->leftJoin('activetl_nodes', 'atl', 'atl.nid = n.nid');
    $query->groupBy('n.nid');
    $query->isNull('atl.value');
    $query = $query->execute();
    $result = $query->fetchAllAssoc('nid');
  }
  return $result;
}
